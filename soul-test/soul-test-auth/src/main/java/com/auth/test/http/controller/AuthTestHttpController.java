package com.auth.test.http.controller;

import com.auth.test.http.vo.UserVo;
import org.dromara.soul.client.common.annotation.SoulClient;
import org.springframework.web.bind.annotation.*;

/**
 * AuthTestHttpController.
 * @author xulj
 */
@RestController
@RequestMapping("/test")
public class AuthTestHttpController {

    /**
     * Find by user id.
     * @param userId the user id
     * @return the string
     */
    @GetMapping("/findByUserId")
    @SoulClient(path = "/test/findByUserId", desc = "查找用户")
    public String findByUserId(@RequestParam("userId") String userId) {
        String result = "find user by userId:" + userId;
        return result;
    }

    /**
     * Add user
     * @param user
     * @return
     */
    @PostMapping("/addUser")
    @SoulClient(path = "/test/addUser", desc = "添加用户")
    public String addUser(@RequestBody UserVo user) {
        String result = "add user:{" + user.getUsername() + ", " + user.getPassword() + "}";
        return result;
    }

    /**
     * Update user
     * @param user
     * @return
     */
    @PutMapping("/updateUser")
    @SoulClient(path = "/test/updateUser", desc = "修改用户")
    public String updateUser(@RequestBody UserVo user) {
        String result = "update user:{" + user.getUsername() + ", " + user.getPassword() + "}";
        return result;
    }

    /**
     * Delete by user id.
     * @param userId the user id
     * @return the string
     */
    @DeleteMapping("/deleteByUserId")
    @SoulClient(path = "/test/deleteByUserId", desc = "删除用户")
    public String deleteByUserId(@RequestParam("userId") String userId) {
        String result = "delete user by userId:" + userId;
        return result;
    }

}
