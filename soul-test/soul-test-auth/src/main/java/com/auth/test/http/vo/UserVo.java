package com.auth.test.http.vo;

import lombok.Data;

@Data
public class UserVo {

    private String username;
    private String password;

}
