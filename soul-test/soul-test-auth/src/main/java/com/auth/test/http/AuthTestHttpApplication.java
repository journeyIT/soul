package com.auth.test.http;

import org.dromara.soul.client.springmvc.configuration.SoulSpringMvcAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * AuthTestHttpApplication.
 * @author xulj
 */
@SpringBootApplication
@ImportAutoConfiguration(SoulSpringMvcAutoConfiguration.class)
public class AuthTestHttpApplication {

    /**
     * main.
     * @param args args
     */
    public static void main(final String[] args) {
        SpringApplication.run(AuthTestHttpApplication.class, args);
    }
}
